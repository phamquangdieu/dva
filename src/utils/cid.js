const ipfsClient = require('ipfs-http-client');
const ipfs = ipfsClient('/ip4/178.128.217.110/tcp/5001');
async function cid(name){
    const files = [{
        path: `/${name}`
    }]
    
    for await (const result of ipfs.add(files)) {
        var cid = result.cid.string
        var src = `/ipfs/${cid}`
        try{
        await ipfs.files.cp([src, `/${name}` ])
        }
        catch(err){
        console.log(err)
        }
    }
    return cid;   
}
export default cid;