import * as siteService from './../services/index'

export default {
    namespace:'sites',
    state:{
        auth:null,
        list:[]
    },
    reducers: {
        save(state, { payload: { auth, list } }) {
          return { ...state, auth, list };
        },
        // fetch_data(state){
        //     const data = siteService.fetch
        //     return {...state, data}
        // }
    },
    effects:{
        *fetch({payload},{ call, put }) {
            const {data}  = yield call(siteService.login);
            const auth = data.data.authorization
            const data_list = yield call(siteService.fetch_list, auth)
            const list = (data_list.data.data)
            yield put({
                type: 'save',
                payload: {
                    auth,
                    list
                },
            });
        },
        *create({payload:{values, auth}},{call, put}){
            const res = yield call(siteService.create, values, auth)
        },
        *reload(action, { put }) {
            yield put({ type: 'fetch'});
        }
    },
    subscriptions: {
        setup({ dispatch, history }) {
            return history.listen(({ pathname}) => {
                if (pathname === '/sites') {
                dispatch({ type: 'fetch'});
                }
            });
        },
    },
}