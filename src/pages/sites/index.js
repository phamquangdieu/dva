import React from 'react'
import SitesModal from './component/SiteModal'
import { connect } from 'dva';
import {Button} from 'antd'
function Sites ({dispatch, auth, list}){
    function handleCreate(values) {
        dispatch({
            type:'sites/create',
            payload:{
                values,
                auth
            }
        })
    }
    return (
        <div>
            <SitesModal auth={auth} onOk={handleCreate}>
                <Button type="primary" >Create Site</Button>
            </SitesModal>
        </div>
    );
}

function mapStateToProps(state) {
    const { auth, list } = state.sites;
    return {
      auth,list
    };
  }
export default connect(mapStateToProps)(Sites);