import React,{ Component } from 'react'
import {connect} from 'dva'
import { Modal, Form, Input } from 'antd';
const FormItem = Form.Item;
class SiteModal extends Component{
    constructor(props){
        super(props)
        this.state={
            visible:false
        }
    }

    showModelHandler = e => {
        if (e) e.stopPropagation();
        this.setState({
          visible: true,
        });
    };

    hideModelHandler = () => {
        this.setState({
          visible: false,
        });
    };
    
    okHandler = () => {
        const { onOk } = this.props;
        this.props.form.validateFields((err, values)=>{
            onOk(values)
        })
        this.hideModelHandler()
    };

    render(){
        const { children } = this.props;
        const { getFieldDecorator } = this.props.form;
        // const { name, email, website } = this.props.record;
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        // console.log(children)
        return (
            <span>
                <span onClick={this.showModelHandler}>{children}</span>
                <Modal
                title="Add Site"
                visible={this.state.visible}
                onOk={this.okHandler}
                onCancel={this.hideModelHandler}
                >
                    <Form layout="horizontal" onSubmit={this.okHandler}>
                        <FormItem {...formItemLayout} label="Name">
                        {getFieldDecorator('name', {
                        })(<Input />)}
                        </FormItem>
                        <FormItem {...formItemLayout} label="Version">
                        {getFieldDecorator('version', {
                        })(<Input />)}
                        </FormItem>
                        <FormItem {...formItemLayout} label="Comment">
                        {getFieldDecorator('comment', {
                        })(<Input />)}
                        </FormItem>
                    </Form>
                </Modal>
            </span>
        );
    }
}


export default Form.create()(SiteModal);