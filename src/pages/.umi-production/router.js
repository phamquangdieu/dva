import React from 'react';
import { Router as DefaultRouter, Route, Switch } from 'react-router-dom';
import dynamic from 'umi/dynamic';
import renderRoutes from 'umi/_renderRoutes';
import { routerRedux } from 'dva/router';


let Router = DefaultRouter;
const { ConnectedRouter } = routerRedux;
Router = ConnectedRouter;


let routes = [
  {
    "path": "/",
    "component": dynamic(() => import(/* webpackChunkName: "src__layouts__index" */'../../layouts/index.js'), {}),
    "routes": [
      {
        "path": "/",
        "exact": true,
        "component": dynamic(() => import(/* webpackChunkName: "src__pages__index" */'../index.js'), {})
      },
      {
        "path": "/sites",
        "exact": true,
        "component": dynamic(() => import(/* webpackChunkName: "src__pages__sites__index" */'../sites/index.js'), {})
      },
      {
        "path": "/sites/services",
        "exact": true,
        "component": dynamic(() => import(/* webpackChunkName: "src__pages__sites__services__index" */'../sites/services/index.js'), {})
      },
      {
        "path": "/sites/models",
        "exact": true,
        "component": dynamic(() => import(/* webpackChunkName: "src__pages__sites__models__index" */'../sites/models/index.js'), {})
      },
      {
        "path": "/sites/component/SiteModal",
        "exact": true,
        "component": dynamic(() => import(/* webpackChunkName: "src__pages__sites__component__SiteModal" */'../sites/component/SiteModal.js'), {})
      },
      {
        "path": "/users",
        "exact": true,
        "component": dynamic(() => import(/* webpackChunkName: "src__pages__users__page" */'../users/page.js'), {})
      }
    ]
  }
];


export default function() {
  return (
<Router history={window.g_history}>
  <Route render={({ location }) =>
    renderRoutes(routes, {}, { location })
  } />
</Router>
  );
}
