import React from 'react';
import { Router as DefaultRouter, Route, Switch } from 'react-router-dom';
import dynamic from 'umi/dynamic';
import renderRoutes from 'umi/_renderRoutes';
import { routerRedux } from 'dva/router';


let Router = DefaultRouter;
const { ConnectedRouter } = routerRedux;
Router = ConnectedRouter;


let routes = [
  {
    "path": "/",
    "component": require('../../layouts/index.js').default,
    "routes": [
      {
        "path": "/",
        "exact": true,
        "component": require('../index.js').default
      },
      {
        "path": "/sites",
        "exact": true,
        "component": require('../sites/index.js').default
      },
      {
        "path": "/sites/services",
        "exact": true,
        "component": require('../sites/services/index.js').default
      },
      {
        "path": "/sites/models",
        "exact": true,
        "component": require('../sites/models/index.js').default
      },
      {
        "path": "/sites/component/SiteModal",
        "exact": true,
        "component": require('../sites/component/SiteModal.js').default
      },
      {
        "path": "/users",
        "exact": true,
        "component": require('../users/page.js').default
      },
      {
        "component": () => React.createElement(require('/Users/apple/Documents/web/dva/examples/user-dashboard/node_modules/umi-build-dev/lib/plugins/404/NotFound.js').default, { pagesPath: 'src/pages', routes: '[{"path":"/","component":"./src/layouts/index.js","routes":[{"path":"/","exact":true,"component":"./src/pages/index.js"},{"path":"/sites","exact":true,"component":"./src/pages/sites/index.js"},{"path":"/sites/services","exact":true,"component":"./src/pages/sites/services/index.js"},{"path":"/sites/models","exact":true,"component":"./src/pages/sites/models/index.js"},{"path":"/sites/component/SiteModal","exact":true,"component":"./src/pages/sites/component/SiteModal.js"},{"path":"/users","exact":true,"component":"./src/pages/users/page.js"}]}]' })
      }
    ]
  }
];


export default function() {
  return (
<Router history={window.g_history}>
  <Route render={({ location }) =>
    renderRoutes(routes, {}, { location })
  } />
</Router>
  );
}
